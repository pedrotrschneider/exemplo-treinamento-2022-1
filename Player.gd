class_name Player
extends KinematicBody2D

var BULLET_SCENE = load("res://Bullet.tscn");

export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;
export(NodePath) onready var barrel_pivot = self.get_node(barrel_pivot) as Position2D;
export(NodePath) onready var barrel = self.get_node(barrel) as Position2D;

var speed : float = 200.0;
var dir : Vector2 = Vector2.ZERO;
var facing_dir : Vector2 = Vector2.RIGHT;


func _physics_process(_delta : float) -> void:
	dir = Vector2.ZERO;
	if(Input.is_action_pressed("player_left")):
		dir.x -= 1;
	if(Input.is_action_pressed("player_right")):
		dir.x += 1;
	if(Input.is_action_pressed("player_up")):
		dir.y -= 1;
	if(Input.is_action_pressed("player_down")):
		dir.y += 1;
	
	if(dir.length() != 0):
		facing_dir = dir;
	
	if(facing_dir.x != 0):
		sprite.scale.x = facing_dir.x;
	
	barrel_pivot.rotation = facing_dir.angle();
	
	var velocity : Vector2 = dir.normalized() * speed;
	# warning-ignore:return_value_discarded
	self.move_and_slide(velocity, Vector2.UP);
	
	if(Input.is_action_just_pressed("player_shoot")):
		var bullet : Bullet = BULLET_SCENE.instance();
		bullet.dir = facing_dir.normalized();
		self.get_parent().add_child(bullet);
		bullet.global_position = barrel.global_position;
		bullet.connect("bulled_destroyed", self, "_on_bullet_destroyed");
	
	if(Input.is_action_just_pressed("delete_bullets")):
		var bullets : Array = self.get_tree().get_nodes_in_group("Bullet");
		for b in bullets:
			if(b is Bullet):
				b.destroy();


func _on_bullet_destroyed(pos : Vector2) -> void:
	print("A bala foi destruída. Essa chama está acontecendo no jogador.")
	print(pos);
