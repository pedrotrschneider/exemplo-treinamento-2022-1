class_name Bullet
extends KinematicBody2D

signal bulled_destroyed(pos);

var speed : float = 500.0;
var dir : Vector2 = Vector2.ZERO;


#func _init(_dir : Vector2) -> void:
#	dir = _dir;


func _ready() -> void:
	self.add_to_group("Bullet");


func _physics_process(delta : float) -> void:
	var collision : KinematicCollision2D = null;
	collision = self.move_and_collide(dir * speed * delta);
	if(collision):
		destroy();


func destroy() -> void:
	self.emit_signal("bulled_destroyed", self.global_position);
	self.queue_free();


func _on_Bullet_bulled_destroyed(pos : Vector2) -> void:
	print("A bala foi destruída. Essa chamada estaá acontecendo na bala");
	print(pos);
